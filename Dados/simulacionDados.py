import numpy as np
import matplotlib.pyplot as plot

dado1= np.random.randint(1,7,1000000)
dado2= np.random.randint(1,7,1000000)

print("Dado 1",dado1)
print("Dado 2",dado2)

salida = np.add(dado1, dado2) 

print("Sumatoria",salida)

intervalos = range(2, 14)

plot.hist(x=salida, bins=intervalos, color='#47C3FF', rwidth=0.85)
plot.title('Histograma - Ocurrencia de la sumatoria')
plot.xlabel('Sumatoria')
plot.ylabel('Frecuencia')
plot.xticks(intervalos)

plot.show()